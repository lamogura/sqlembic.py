# -*- coding: utf-8 -*-
import sqlalchemy as sa
from sqlalchemy.ext.declarative import declared_attr


class CommonMixin(object):
    """Base Mixin for SQLAlchemy model to provide common functionality"""
    @declared_attr
    def __tablename__(cls):
        return cls.__name__

    id = sa.Column(sa.Integer, primary_key=True)

    def __repr__(self):
        return '<{} id={}>'.format(self.__class__.__name__, self.id)


class TimestampMixin(object):
    """Mixin for SQLAlchemy model to give timestamp columns"""
    created_at = sa.Column(sa.DateTime, server_default=sa.func.now())
    updated_at = sa.Column(
        sa.DateTime, server_default=sa.func.now(), onupdate=sa.func.now())
