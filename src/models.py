# -*- coding: utf-8 -*-
import sqlalchemy as sa
from .database import Base
from .helpers import CommonMixin, TimestampMixin


__all__ = ['DummyModel', 'DummyTimestampModel']


class DummyModel(CommonMixin, Base):
    some_name = sa.Column(sa.Unicode(80))


class DummyTimestampModel(TimestampMixin, CommonMixin, Base):
    some_number = sa.Column(sa.Integer())
