# -*- coding: utf-8 -*-
import logging
from os import getenv


class Config(object):
    DEBUG = False
    LOG_FORMAT = "%(levelname)s [%(asctime)s] (%(filename)s:%(lineno)s) %(message)s"
    LOG_LEVEL = logging.DEBUG


class DevelopmentConfig(Config):
    DEBUG = True
    DATABASE_URI = 'postgresql://localhost/sqlembic-dev'


class TestingConfig(Config):
    TESTING = True
    DATABASE_URI = 'postgresql://localhost/sqlembic-test'


class ProductionConfig(Config):
    DATABASE_URI = getenv('DATABASE_URL')


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
