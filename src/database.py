# -*- coding: utf-8 -*-
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from os import getenv
from .config import config


Base = declarative_base()

# create an engine
cfg = config[getenv('SQLEMBIC_CONFIG_ENV', 'default')]
engine = create_engine(cfg.DATABASE_URI)

# create a configured "Session" class
Session = sessionmaker(bind=engine)
