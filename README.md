# Skeleton sqlalchemy + alembic python project

## installation

### install pipenv (if needed)
```bash
> pip3 install pipenv
```

### install deps
```bash
> pipenv install
```

### activate shell
```bash
> pipenv shell
```

### create tables
```bash
> alembic upgrade head
```
`Note: you need to create "sqlembic-dev" database before creating tables`

## run tests
```bash
> SQLEMBIC_CONFIG_ENV=testing pytest tests
```
`Note: you need to create "sqlembic-test" database before running tests`