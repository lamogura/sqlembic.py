# -*- coding: utf-8 -*-
from os import getenv
from os.path import dirname, abspath
import sys
import pytest
sys.path.append(dirname(dirname(abspath(__file__))))

from src.models import *


@pytest.fixture(scope="session")
def engine(request):
    """Engine configuration.
    See http://docs.sqlalchemy.org/en/latest/core/engines.html
    for more details.
    :sqlalchemy_connect_url: Connection URL to the database. E.g
    postgresql://scott:tiger@localhost:5432/mydatabase 
    :app_config: Path to a ini config file containing the sqlalchemy.url
    config variable in the DEFAULT section.
    :returns: Engine instance
    """

    from src.database import engine

    def fin():
        print("Disposing engine")
        engine.dispose()

    request.addfinalizer(fin)
    return engine


@pytest.fixture(scope="module")
def connection(request, engine):
    connection = engine.connect()

    from src.database import Base

    Base.metadata.create_all(engine)

    def fin():
        print("Closing connection")
        connection.close()

    request.addfinalizer(fin)
    return connection


@pytest.fixture()
def transaction(request, connection):
    """Will start a transaction on the connection. The connection will
    be rolled back after it leaves its scope."""
    transaction = connection.begin()

    def fin():
        print("Rollback")
        transaction.rollback()

    request.addfinalizer(fin)
    return connection


@pytest.fixture()
def dbsession(request, transaction):
    from sqlalchemy.orm import sessionmaker
    return sessionmaker()(bind=transaction)
