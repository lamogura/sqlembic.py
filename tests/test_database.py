# -*- coding: utf-8 -*-
import sys
from os.path import dirname, abspath
sys.path.append(dirname(dirname(abspath(__file__))))

from src.models import *


def test_database(dbsession):
    x = DummyModel(some_name="candy")
    y = DummyTimestampModel(some_number=2)
    dbsession.add_all([x, y])
    dbsession.commit()
    assert x.id is not None and y.id is not None
